let gulp = require('gulp'),
    concatCss = require('gulp-concat-css'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    uncss = require('gulp-uncss'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    server = require('gulp-server-livereload'),
    del = require('del');
    watch = require('gulp-watch');

gulp.task('clean', function(){
   del.sync('dist/js');
   del.sync('dist/css');
   del.sync('dist/img');
   del.sync('dist/img')
});

gulp.task('cssBuild', function () {
    return gulp.src('app/css/*.css')
        .pipe(concatCss("app/css/main.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename("main.min.css"))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('scriptsBuild', function () {
    return gulp.src('app/js/*.js')
        .pipe(concat('dist/js/main.js'))
        //Compact js code
        // .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('imgBuild', function () {
    gulp.src('app/images/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5
        }))
        .pipe(gulp.dest('dist/img'))
});

gulp.task('live', function() {
    gulp.src('dist')
        .pipe(server({
            livereload: true,
            defaultFile: 'index.html',
            open: false
        }));
});

gulp.task('watch', ['scriptsBuild', 'cssBuild', 'imgBuild'], function() {
    gulp.watch('app/js/*.js', ['scriptsBuild']);
    gulp.watch('app/css/*.css',['cssBuild']);
    gulp.watch('app/img/*',['imgBuild']);
    gulp.watch('dist/index.html')
});

gulp.task('default', ['scriptsBuild', 'cssBuild','imgBuild','watch', 'live']);

gulp.task('build', ['scriptsBuild', 'cssBuild','imgBuild']);