## Getting Started

After run command gulp will start compiling  
```
gulp
```

####Task comand list
*   Concat all task ad run they 
```
gulp.task('default', ['scriptsBuild', 'sassBuild', 'cssBuild','imgBuild','watch', 'live']);

```
comand
```
gulp
```

*   Build 

```
gulp.task('build', ['scriptsBuild', 'sassBuild', 'cssBuild','imgBuild']);
```
comand
```
gulp build
```


*   Clean dist  
```
gulp.task('clean', function(){
   del.sync('dist/js');
   del.sync('dist/css');
   del.sync('dist/img');
   del.sync('dist/img')
});
```
comand
```
gulp clean
```

####Task list
*   Sass build
```
gulp.task('sassBuild', function () {
    return gulp.src('app/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concatCss("app/css/sass.css"))
        .pipe(gulp.dest(''));
});
```
*   Css build
```
gulp.task('cssBuild', function () {
    return gulp.src('app/css/*.css')
        .pipe(concatCss("app/css/main.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename("main.min.css"))
        //.pipe(uncss({html: ['html/index.html']}))
        .pipe(gulp.dest('dist/css'));
});
```

*   JavaScript build
```
gulp.task('scriptsBuild', function () {
    return gulp.src('app/js/*.js')
        .pipe(concat('dist/js/main.js'))
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('./dist/js'));
});
```

*   Image compacting
```
gulp.task('imgBuild', function () {
    gulp.src('app/images/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5
        }))
        .pipe(gulp.dest('dist/img'))
});
```
*   Start live watching at: `http://localhost:8000/`
```
gulp.task('live', function() {
    gulp.src('dist')
        .pipe(server({
            livereload: true,
            defaultFile: 'index.html',
            open: false
        }));
});
```
*   Watch file changing and recompiling
```
gulp.task('watch', ['scriptsBuild', 'sassBuild', 'cssBuild', 'scriptsBuild', 'imgBuild'], function() {
    gulp.watch('app/js/*.js', ['scriptsBuild']);
    gulp.watch('app/css/*.css',['sassBuild', 'cssBuild']);
    gulp.watch('app/img/*',['imgBuild']);
    gulp.watch('dist/index.html')
});
```

*   Concat all task ad run they 
```
gulp.task('default', ['scriptsBuild', 'sassBuild', 'cssBuild','imgBuild','watch', 'live']);

```
```
gulp
```

*   Build 

```
gulp.task('build', ['scriptsBuild', 'sassBuild', 'cssBuild','imgBuild']);
```
```
gulp build
```


*   Clean dist  
```
gulp.task('clean', function(){
   del.sync('dist/js');
   del.sync('dist/css');
   del.sync('dist/img');
   del.sync('dist/img')
});
```
```
gulp clean
```